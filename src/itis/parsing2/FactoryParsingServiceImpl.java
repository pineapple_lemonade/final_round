package itis.parsing2;

import itis.parsing2.annotations.Concatenate;
import itis.parsing2.annotations.NotBlank;

import java.io.*;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;

public class FactoryParsingServiceImpl implements FactoryParsingService {

    @Override
    public Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
	    ArrayList<String> list = new ArrayList<>();
	    FactoryParsingException factoryParsingException = new FactoryParsingException("error", new ArrayList<>());
	    try {
		    File file = new File(factoryDataDirectoryPath);
		    File[] files = file.listFiles();
		    for (File file1 : files) {
			    FileReader fileReader = new FileReader(file1);
			    BufferedReader bufferedReader = new BufferedReader(fileReader);
			    while (true) {
				    String string = bufferedReader.readLine();
				    if (string == null) {
					    bufferedReader.close();
					    break;
				    }
				    list.add(string);
			    }
		    }
		    list = (ArrayList<String>) list.stream()
				    .filter((o) -> !o.contains("---"))
				    .map((o) -> o.replaceAll("\"", ""))
				    //.map((o) -> o.replaceAll(":", ""))
				    .collect(Collectors.toList());
	    } catch (FileNotFoundException e) {
		    e.printStackTrace();
	    } catch (IOException e) {
		    e.printStackTrace();
	    }
	    Class<Factory> factoryClass = Factory.class;
	    Constructor<Factory> declaredConstructor = factoryClass.getDeclaredConstructor();
	    Factory factory = declaredConstructor.newInstance();
	    ArrayList<Field> fields = new ArrayList<>();
	    fields = (ArrayList<Field>) Arrays.stream(factoryClass.getDeclaredFields())
			    .peek(o -> o.setAccessible(true))
			    .collect(Collectors.toList());

	    for (Field field : fields) {
	    	boolean isError = false;
		    Annotation[] annotations = field.getAnnotations();
		    String strToSet = "";
		    if (Arrays.stream(annotations).anyMatch(annotation -> annotation instanceof NotBlank)) {
			    boolean isPassed = false;
			    for (int i = 0; i < list.size(); i++) {
				    String[] split = list.get(i).split(":");
				    if(split.length >= 2) {
					    if (list.get(i).contains(field.getName()) && !(split[1].equals("") || split[1].equals(" "))) {
						    strToSet = split[1];
						    isPassed = true;
					    }
				    }
			    }
			    if (isPassed) {
				    field.set(factory, strToSet);
			    } else {
			    	isError = true;
			    	factoryParsingException.getValidationErrors().add(new FactoryParsingException.FactoryValidationError(field.getName(), "NotBlank exception"));
			    }
		    }
		    if (Arrays.stream(annotations).anyMatch(annotation -> annotation instanceof Concatenate)) {
			    Concatenate concatenate = null;
			    boolean isPassed = false;
			    for (int i = 0; i < annotations.length; i++) {
				    if (annotations[i] instanceof Concatenate) {
					    concatenate = (Concatenate) annotations[i];
				    }
			    }
			    String[] strings = concatenate.fieldNames();
			    ArrayList<String> listOfConcatinations = new ArrayList<>();
			    for (int i = 0; i < list.size(); i++) {
				    for (int j = 0; j < strings.length; j++) {
					    if (list.get(i).contains(strings[j])) {
						    listOfConcatinations.add(list.get(i));
					    }
				    }
			    }
			    if (listOfConcatinations.size() == strings.length) {
				    isPassed = true;
			    }
			    if (isPassed) {
				    for (int i = 0; i < list.size(); i++) {
					    for (int j = 0; j < listOfConcatinations.size(); j++) {
						    if (list.get(i).contains(listOfConcatinations.get(j))) {
							    String[] split = list.get(i).split(":");
							    strToSet += split[1] + concatenate.delimiter();
						    }
					    }
				    }
				    field.set(factory, strToSet);
			    } else {
			    	isError = true;
				    factoryParsingException.getValidationErrors().add(new FactoryParsingException.FactoryValidationError(field.getName(), "concatenate error"));
			    }
		    }
		    if (field.getName() == "departments") {
		    	boolean isPassed = false;
		    	ArrayList<String> departments = new ArrayList<>();
			    for (int i = 0; i < list.size(); i++) {
				    if (list.get(i).contains("departments")) {
				    	departments.add(list.get(i));
				    	isPassed = true;
				    }
			    }
			    if(isPassed){
			    	ArrayList<String> department = new ArrayList<>();
				    for (int i = 0; i < departments.size(); i++) {
					    String[] split = departments.get(i).split(":");
					    String[] split1 = split[1].split(",");
					    department.add(split1[0].substring(1));
					    department.add(split1[1].substring(1));
				    }
				    field.set(factory,department);
			    } else {
			    	isError = true;
				    factoryParsingException.getValidationErrors().add(new FactoryParsingException.FactoryValidationError(field.getName(),"not found"));
			    }
		    }
		    if (field.getName() == "amountOfWorkers") {
		    	boolean isPassed = false;
		    	Long longToSet = null;
			    for (int i = 0; i < list.size(); i++) {
					if(list.get(i).contains("amountOfWorkers")){
						isPassed = true;
						String[] split = list.get(i).split(":");
						longToSet = Long.parseLong(split[1]);
					}
			    }
			    if(isPassed){
			    	field.set(factory,longToSet);
			    } else {
			    	isError = true;
			    	factoryParsingException.getValidationErrors().add(new FactoryParsingException.FactoryValidationError(field.getName(),"not found"));
			    }
		    }
		    if(field.getName() == "description"){
			    boolean isPassed = false;
			    for (int i = 0; i < list.size(); i++) {
				    if(list.get(i).contains("description")){
					    isPassed = true;
					    String[] split = list.get(i).split(":");
					    strToSet = split[1];
				    }
			    }
			    if(isPassed){
				    field.set(factory,strToSet);
			    } else {
				    isError = true;
				    factoryParsingException.getValidationErrors().add(new FactoryParsingException.FactoryValidationError(field.getName(),"not found"));
			    }
		    }
		    if(isError){
			    factoryParsingException.printStackTrace();
			    throw factoryParsingException;
		    }
	    }
	    return factory;
    }
}
