package itis.parsing2;

import java.lang.reflect.InvocationTargetException;

interface FactoryParsingService {

    Factory parseFactoryData(String factoryDataDirectoryPath) throws FactoryParsingException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException;

}
